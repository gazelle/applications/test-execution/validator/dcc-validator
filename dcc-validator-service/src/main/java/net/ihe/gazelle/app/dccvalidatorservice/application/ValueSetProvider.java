package net.ihe.gazelle.app.dccvalidatorservice.application;


import net.ihe.gazelle.app.dccvalidator.business.Concept;
import net.ihe.gazelle.app.dccvalidator.business.DCCValueSet;

import java.util.List;
import java.util.Map;

public interface ValueSetProvider {

    Map<DCCValueSet, List<Concept>> getDccValueSets();
}
