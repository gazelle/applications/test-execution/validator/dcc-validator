package net.ihe.gazelle.app.dccvalidatorservice.application;

import net.ihe.gazelle.app.dccvalidator.application.AbstractValidator;
import net.ihe.gazelle.app.dccvalidator.application.BasicDCCValidator;
import net.ihe.gazelle.modelapi.validation.business.InvalidObjectException;
import net.ihe.gazelle.modelapi.validation.business.SeverityLevel;
import net.ihe.gazelle.modelapi.validation.business.UnkownValidatorException;
import net.ihe.gazelle.modelapi.validation.business.ValidationOverview;
import net.ihe.gazelle.modelapi.validation.business.ValidationReport;
import net.ihe.gazelle.modelapi.validation.business.Validator;
import net.ihe.gazelle.modelapi.validation.interlay.ws.rest.ValidationServiceDTO;
import net.ihe.gazelle.modelapi.validation.interlay.dto.ValidationReportDTO;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class DigitalCovidCertificateValidationService implements ValidationServiceDTO {

   private Set<AbstractValidator> validators = new HashSet<>();

   public DigitalCovidCertificateValidationService(CryptoProvider cryptoProvider, ValueSetProvider valueSetProvider) {
      validators.add(new BasicDCCValidator(cryptoProvider.getTrustedDCCIssuers(), valueSetProvider.getDccValueSets()));
   }

   @Override
   public String getName() {
      return "Digital COVID Certificate Validation Service";
   }

   @Override
   public List<Validator> getValidators() {
      return new ArrayList<>(validators);
   }

   @Override
   public List<Validator> getValidators(String domain) {
      throw new UnsupportedOperationException("Method getValidators(String domain) not implemented");
   }

   @Override
   public List<String> getSupportedMediaTypes() {
      throw new UnsupportedOperationException("Method getSupportedMediaTypes() not implemented");
   }

   @Override
   public void validate(byte[] object, String validatorKeyword) throws UnkownValidatorException, InvalidObjectException {
      getValidator(validatorKeyword).validate(object);
   }

   @Override
   public ValidationReportDTO validateWithReport(byte[] object, String validatorKeyword) throws UnkownValidatorException {
      ValidationReport validationReport = getValidator(validatorKeyword).validateWithReport(object, createValidationOverview(validatorKeyword));
      return new ValidationReportDTO(validationReport, SeverityLevel.INFO);
   }

   private AbstractValidator getValidator(String validatorKeyword) throws UnkownValidatorException {
      return validators.stream().filter(v -> v.getKeyword().equals(validatorKeyword)).findFirst().orElseThrow(() ->
            new UnkownValidatorException(String.format("Unkown Validator '%s'", validatorKeyword)));
   }

   private ValidationOverview createValidationOverview(String validatorKeyword) {
      return  new ValidationOverview(
            "Copyright 2021 IHE International Gazelle\n" +
                  "\n" +
                  "   Licensed under the Apache License, Version 2.0 (the \"License\");\n" +
                  "   you may not use this file except in compliance with the License.\n" +
                  "   You may obtain a copy of the License at\n" +
                  "\n" +
                  "       http://www.apache.org/licenses/LICENSE-2.0\n" +
                  "\n" +
                  "   Unless required by applicable law or agreed to in writing, software\n" +
                  "   distributed under the License is distributed on an \"AS IS\" BASIS,\n" +
                  "   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.\n" +
                  "   See the License for the specific language governing permissions and\n" +
                  "   limitations under the License.",
            getName(),
            "1.0.0-SNAPSHOT",
            validatorKeyword);
   }
}
