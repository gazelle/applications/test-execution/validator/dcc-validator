package net.ihe.gazelle.app.dccvalidatorservice.adapter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.StringReader;

/**
 * <p>XMLParser class.</p>
 *
 * @author abe
 * @version 1.0: 23/07/18
 */

public class XMLParser {

    private static final Logger LOG = LoggerFactory.getLogger(XMLParser.class);

    private XMLParser() {

    }

    public static Document parseStringAsDocument(String xmlString) {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder;
        try {
            builder = factory.newDocumentBuilder();
            return builder.parse(new InputSource(new StringReader(xmlString)));
        } catch (Exception e) {
            LOG.error("Cannot parse string '" + xmlString + "' as an XML Document");
            return null;
        }
    }
}
