package net.ihe.gazelle.app.dccvalidatorservice.adapter;

import net.ihe.gazelle.app.dccvalidator.business.DCCValueSet;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import se.digg.dgc.valueset.v1.ValueSet;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class ValueSetJsonToSVS {

    public static final String URL_BASE = "https://raw.githubusercontent.com/ehn-dcc-development/ehn-dcc-valuesets/";
    public static final String VERSION = "2.7.0";

    public static List<ValueSet> getDccValueSets() {
        List<ValueSet> valueSetList = new ArrayList<>();
        for (DCCValueSet dccValueSet : DCCValueSet.values()) {
            StringBuilder urlString = new StringBuilder();
            urlString.append(URL_BASE);
            urlString.append(VERSION);
            urlString.append("/");
            urlString.append(dccValueSet.fileName);
            try {
                URL url = new URL(urlString.toString());
                InputStream is = url.openStream();
                valueSetList.add(new ValueSet(is));
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return valueSetList;
    }

    public static void main(String[] args) throws ParserConfigurationException, TransformerException {
        int i = 1;
        for (ValueSet valueSet : getDccValueSets()) {
            DocumentBuilderFactory documentFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder documentBuilder = documentFactory.newDocumentBuilder();
            Document document = documentBuilder.newDocument();

            // root element
            Element root = document.createElement("RetrieveValueSetResponse");
            root.setAttribute("xmlns", "urn:ihe:iti:svs:2008");
            document.appendChild(root);

            Element valueSetElem = document.createElement("ValueSet");
            valueSetElem.setAttribute("displayName", "DCC " + valueSet.getId());
            valueSetElem.setAttribute("version", valueSet.getDate().toString());
            valueSetElem.setAttribute("id", "1.1.1.1.1." + i);
            root.appendChild(valueSetElem);

            Element conceptList = document.createElement("ConceptList");
            conceptList.setAttribute("xml:lang", "en");
            valueSetElem.appendChild(conceptList);

            for (var entry : valueSet.getValues().entrySet()) {
                Element concept = document.createElement("Concept");
                concept.setAttribute("code", entry.getKey());
                if (entry.getValue().getDisplay() != null && !entry.getValue().getDisplay().isEmpty()) {
                    concept.setAttribute("displayName", entry.getValue().getDisplay());
                }
                if (entry.getValue().getSystem() != null && !entry.getValue().getSystem().isEmpty()) {
                    concept.setAttribute("codeSystemName", entry.getValue().getSystem());
                }
                if (entry.getValue().getVersion() != null && !entry.getValue().getVersion().isEmpty()) {
                    concept.setAttribute("codeSystemVersion", entry.getValue().getVersion());
                }
                concept.setAttribute("codeSystem", "1.1.1.1");
                conceptList.appendChild(concept);
            }

            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource domSource = new DOMSource(document);
            StreamResult streamResult = new StreamResult(new File("dcc-validator-service/src/test/resources/valuesets/" + valueSet.getId() + ".xml"));
            transformer.transform(domSource, streamResult);

            i++;

        }


    }


}
