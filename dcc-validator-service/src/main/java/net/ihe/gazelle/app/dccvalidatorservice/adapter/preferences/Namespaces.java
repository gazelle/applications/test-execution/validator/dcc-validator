package net.ihe.gazelle.app.dccvalidatorservice.adapter.preferences;

/**
 * Enumeration of all Preferences defined by the application. It allows to centralize the values of Preferences' keys as well as the
 * namespace where they are defined.
 */
public enum Namespaces {
    DEPLOYMENT("java:/app/gazelle/dcc-validator/operational-preferences");


    private final String value;

    /**
     * Default constructor for the class. Defines the value of the value in the JNDI context. This aims to be used by the Preferences Module.
     *
     * @param value value of the value in the JNDI context.
     */
    Namespaces(String value) {
        this.value = value;
    }

    /**
     * Getter for the value property.
     *
     * @return the value of the value property.
     */
    public String getValue() {
        return value;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return value;
    }
}
