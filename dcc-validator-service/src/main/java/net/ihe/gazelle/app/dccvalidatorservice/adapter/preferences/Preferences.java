package net.ihe.gazelle.app.dccvalidatorservice.adapter.preferences;

public enum Preferences {
    TRUSTSTORE_PATH(Namespaces.DEPLOYMENT, "truststore.path", null),
    TRUSTSTORE_PASSWORD(Namespaces.DEPLOYMENT, "truststore.password", null),
    SVS_SIMULATOR_URL(Namespaces.DEPLOYMENT, "svssimulator.url", "svs_repository_url");

    private final Namespaces namespace;
    private final String key;
    private final String daoKey;

    /**
     * Default constructor for the class. Defines the value of the Preference's key. It can then be used throughout the application to retrieve the
     * preference value.
     *
     * @param namespace value of the namespace where the Preference is defined.
     * @param key       value of the Preference's key.
     */
    Preferences(Namespaces namespace, String key, String daoKey) {
        this.namespace = namespace;
        this.key = key;
        this.daoKey = daoKey;
    }

    /**
     * Getter for the namespace property.
     *
     * @return the value of the namespace property.
     */
    public Namespaces getNamespace() {
        return namespace;
    }

    /**
     * Getter for the key property.
     *
     * @return the value of the key property.
     */
    public String getKey() {
        return key;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return key;
    }

    public static Preferences getPreferenceByDaokey(String daoKey) {
        for (Preferences preferences : Preferences.values()) {
            if (daoKey.equals(preferences.daoKey)) {
                return preferences;
            }
        }
        return null;
    }
}
