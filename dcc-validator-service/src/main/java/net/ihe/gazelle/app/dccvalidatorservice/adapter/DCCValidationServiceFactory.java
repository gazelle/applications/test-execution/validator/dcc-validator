package net.ihe.gazelle.app.dccvalidatorservice.adapter;

import net.ihe.gazelle.app.dccvalidatorservice.application.CryptoProvider;
import net.ihe.gazelle.app.dccvalidatorservice.application.DigitalCovidCertificateValidationService;
import net.ihe.gazelle.app.dccvalidatorservice.application.ValueSetProvider;

import javax.enterprise.inject.Produces;
import javax.inject.Inject;

public class DCCValidationServiceFactory {

   @Inject
   CryptoProvider cryptoProvider;

   @Inject
   ValueSetProvider valueSetProvider;

   @Produces
   public DigitalCovidCertificateValidationService getDCCValidationService() {
      return new DigitalCovidCertificateValidationService(cryptoProvider, valueSetProvider);
   }

}
