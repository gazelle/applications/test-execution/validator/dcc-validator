package net.ihe.gazelle.app.dccvalidatorservice.adapter.preferences;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import net.ihe.gazelle.framework.preferencesmodelapi.application.OperationalPreferencesClientApplication;

public class OperationalPreferencesDcc implements OperationalPreferencesClientApplication {
    @Override
    public Map<String, List<String>> wantedMandatoryPreferences() {
        Map<String, List<String>> mandatoryPreferences = new HashMap<>();
        List<String> deploymentPreferences = new ArrayList<>();

        deploymentPreferences.add(Preferences.SVS_SIMULATOR_URL.toString());
        deploymentPreferences.add(Preferences.TRUSTSTORE_PATH.toString());
        deploymentPreferences.add(Preferences.TRUSTSTORE_PASSWORD.toString());

        mandatoryPreferences.put(Namespaces.DEPLOYMENT.getValue(), deploymentPreferences);
        return mandatoryPreferences;
    }
}
