package net.ihe.gazelle.app.dccvalidatorservice.adapter.preferences;

import net.ihe.gazelle.framework.preferencesmodelapi.application.NamespaceException;
import net.ihe.gazelle.framework.preferencesmodelapi.application.OperationalPreferencesService;
import net.ihe.gazelle.framework.preferencesmodelapi.application.PreferenceException;

import javax.inject.Inject;

public class PreferencesLoader {

    private String truststorePath;
    private String truststorePassword;
    private String svsSimulatorUrl;


    OperationalPreferencesService operationalPreferencesService;

    @Inject
    public PreferencesLoader(OperationalPreferencesService operationalPreferencesService) {
        this.operationalPreferencesService = operationalPreferencesService;
    }

    public String getTruststorePath() {
        try {
            return operationalPreferencesService.
                    getStringValue(Preferences.TRUSTSTORE_PATH.getNamespace().getValue(),
                            Preferences.TRUSTSTORE_PATH.getKey());
        } catch (PreferenceException | NamespaceException  e) {
            e.printStackTrace();
            return  null;
        }
    }

    public String getTruststorePassword() {
        try {
            return operationalPreferencesService.
                    getStringValue(Preferences.TRUSTSTORE_PASSWORD.getNamespace().getValue(),
                            Preferences.TRUSTSTORE_PASSWORD.getKey());
        } catch (PreferenceException | NamespaceException  e) {
            e.printStackTrace();
            return  null;
        }
    }

    public String getSvsSimulatorUrl() {
        try {
            return operationalPreferencesService.
                    getStringValue(Preferences.SVS_SIMULATOR_URL.getNamespace().getValue(),
                            Preferences.SVS_SIMULATOR_URL.getKey());
        } catch (PreferenceException | NamespaceException e) {
            e.printStackTrace();
            return null;
        }
    }
}
