package net.ihe.gazelle.app.dccvalidatorservice.application;

import java.security.cert.X509Certificate;
import java.util.List;

public interface CryptoProvider {

   List<X509Certificate> getTrustedDCCIssuers();

}
