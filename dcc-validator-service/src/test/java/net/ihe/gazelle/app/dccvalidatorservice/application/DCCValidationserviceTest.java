package net.ihe.gazelle.app.dccvalidatorservice.application;

import net.ihe.gazelle.app.dccvalidatorservice.adapter.JKSCryptoProvider;
import net.ihe.gazelle.app.dccvalidatorservice.adapter.ValueSetProviderForUT;
import net.ihe.gazelle.modelapi.validation.business.InvalidObjectException;
import net.ihe.gazelle.modelapi.validation.business.UnkownValidatorException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import java.security.KeyStoreException;

public class DCCValidationserviceTest {

   DigitalCovidCertificateValidationService validationService;

   @BeforeEach
   public void setup() throws KeyStoreException {
      validationService = new DigitalCovidCertificateValidationService(new JKSCryptoProvider(
            DCCValidationserviceTest.class.getClassLoader().getResource("keystore.jks").getPath(), "password"
      ), new ValueSetProviderForUT());
   }

   @Test
   public void testGetValidator() {
      Assertions.assertEquals("basic 1.0.0", validationService.getValidators().get(0).getKeyword());
   }

   @ParameterizedTest
   @ValueSource(strings = { "fr_recovery_ok", "fr_vaccin_ok" })
   public void validateTest(String testDataName) {
      JSONTestDataProvider testDataProvider = new JSONTestDataProvider(testDataName);
      validationService = new DigitalCovidCertificateValidationService(testDataProvider, new ValueSetProviderForUT());
      Assertions.assertDoesNotThrow(() -> validationService.validate(testDataProvider.getBarcodeImage(), "basic 1.0.0"));
   }

   @ParameterizedTest
   @ValueSource(strings = { "fr_test-pcr_ok" })
   public void validateTestExpired(String testDataName) {
      JSONTestDataProvider testDataProvider = new JSONTestDataProvider(testDataName);
      validationService = new DigitalCovidCertificateValidationService(testDataProvider, new ValueSetProviderForUT());
      Assertions.assertThrows(InvalidObjectException.class, () -> validationService.validate(testDataProvider.getBarcodeImage(), "basic 1.0.0"));
   }

   @Test
   public void unknownValidatorTest() {
      Assertions.assertThrows(UnkownValidatorException.class, () -> validationService.validate(null, "unknown validator"));
   }

}
