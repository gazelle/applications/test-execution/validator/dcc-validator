package net.ihe.gazelle.app.dccvalidatorservice.application;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.json.JsonMapper;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.Arrays;
import java.util.Base64;
import java.util.List;

public class JSONTestDataProvider implements CryptoProvider {

   private static final String TESTDATA_RESOURCE_DIR = "certificates";
   private static final String BARCODE_IMAGE_EXTENSION = ".png";
   private static final String TEST_METADATA_EXTENSION = ".json";
   private static final String PEM_EXTENSION = ".pem";
   private static final String PEM_CERTIFICATE_HEADER = "-----BEGIN CERTIFICATE-----\n";
   private static final String PEM_CERTIFICATE_FOOTER = "\n-----END CERTIFICATE-----";

   public String testDataName;

   public JSONTestDataProvider(String testDataName) {
      this.testDataName = testDataName;
   }

   @Override
   public List<X509Certificate> getTrustedDCCIssuers() {
      try (InputStream is = new ByteArrayInputStream(getIssuerPEMBytes())) {
         return Arrays.asList((X509Certificate) CertificateFactory.getInstance("X.509").generateCertificate(is));
      } catch (IOException | CertificateException e) {
         throw new RuntimeException(e);
      }
   }

   public byte[] getBarcodeImage() throws IOException {
      return getResourceAsStream(buildResourcePath(testDataName, BARCODE_IMAGE_EXTENSION)).readAllBytes();
   }

   public String getBase64BarcodeImage() throws IOException {
      return Base64.getEncoder().encodeToString(getBarcodeImage());
   }

   private JsonNode getJsonTestMetadata(String testDataName) throws IOException {
      ObjectMapper jsonMapper = new JsonMapper();
      return jsonMapper.readTree(getResourceAsStream(buildResourcePath(testDataName, TEST_METADATA_EXTENSION)));
   }

   private String buildResourcePath(String fileName, String fileExtension) {
      return TESTDATA_RESOURCE_DIR + File.separator + fileName + fileExtension;
   }

   private InputStream getResourceAsStream(String ressourcePath) {
      return JSONTestDataProvider.class.getClassLoader().getResourceAsStream(ressourcePath);
   }

   public byte[] getIssuerPEMBytes() throws IOException {
      JsonNode rootNode = getJsonTestMetadata(testDataName);

      StringBuilder pemBuilder = new StringBuilder(PEM_CERTIFICATE_HEADER);
      pemBuilder.append(rootNode.get("TESTCTX").get("CERTIFICATE").asText());
      pemBuilder.append(PEM_CERTIFICATE_FOOTER);

      return pemBuilder.toString().getBytes(StandardCharsets.UTF_8);
   }

   public static void main(String args[]) throws IOException {
      for(String testDataName : new String[]{"fr_recovery_ok", "fr_test-pcr_ok", "fr_vaccin_ok"} ) {
         try (FileOutputStream fout = new FileOutputStream(
               "dcc-validator-service/src/test/resources/certificates/" + testDataName + ".pem")) {
            fout.write(new JSONTestDataProvider(testDataName).getIssuerPEMBytes());
         }
      }
   }
}
