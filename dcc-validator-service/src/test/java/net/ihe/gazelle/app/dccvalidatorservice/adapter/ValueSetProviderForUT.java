package net.ihe.gazelle.app.dccvalidatorservice.adapter;

import net.ihe.gazelle.app.dccvalidator.business.Concept;
import net.ihe.gazelle.app.dccvalidator.business.DCCValueSet;
import net.ihe.gazelle.app.dccvalidatorservice.application.ValueSetProvider;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ValueSetProviderForUT implements ValueSetProvider {

    @Override
    public Map<DCCValueSet, List<Concept>> getDccValueSets() {
        Map<DCCValueSet, List<Concept>> vs = new HashMap<>();
        List<Concept> conceptList = new ArrayList<>();
        Concept concept = new Concept("840539006", "COVID-19", "1.1.1.1");
        conceptList.add(concept);
        vs.put(DCCValueSet.DISEASE_AGENT_TARGETED, conceptList);

        concept = new Concept("LP217198-3", "Nucleic acid amplification with probe detection", "1.1.1.1");
        conceptList.add(concept);
        vs.put(DCCValueSet.TEST_TYPE, conceptList);

        concept = new Concept("2108", "ACON Laboratories, Inc, Flowflex SARS-CoV-2 Antigen rapid test", "1.1.1.1");
        conceptList.add(concept);
        vs.put(DCCValueSet.TEST_MANF, conceptList);

        concept = new Concept("260415000", "Not detected", "1.1.1.1");
        conceptList.add(concept);
        vs.put(DCCValueSet.TEST_RESULT, conceptList);

        concept = new Concept("FR", "France", "1.1.1.1");
        conceptList.add(concept);
        vs.put(DCCValueSet.COUNTRY_2_CODE, conceptList);

        return vs;
    }
}
