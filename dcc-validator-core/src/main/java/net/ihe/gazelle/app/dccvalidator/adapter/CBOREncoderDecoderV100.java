package net.ihe.gazelle.app.dccvalidator.adapter;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.dataformat.cbor.databind.CBORMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.fasterxml.jackson.datatype.jsr310.deser.InstantDeserializer;
import com.upokecenter.cbor.CBORObject;
import net.ihe.gazelle.app.dccvalidator.business.DecodingException;
import net.ihe.gazelle.app.dccvalidator.business.EncodedCBORDCC;
import net.ihe.gazelle.app.dccvalidator.business.EncoderDecoder;
import net.ihe.gazelle.app.dccvalidator.business.EncodingException;
import net.ihe.gazelle.app.dccvalidator.business.ValidationException;
import net.ihe.gazelle.modelapi.dcc.v1_0_0.Eudgc;
import net.ihe.gazelle.modelapi.validation.business.ConstraintPriority;
import net.ihe.gazelle.modelapi.validation.business.ValidationReport;

import java.io.IOException;
import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAccessor;
import java.util.function.BiFunction;
import java.util.function.Function;

//FIXME Split into 2 EncoderDecoder : CBOR <-> JSON, and JSON <-> POJO
public class CBOREncoderDecoderV100 implements EncoderDecoder<EncodedCBORDCC, Eudgc> {

   private static final String CBOR_CONSTRAINT = "DCC must be conforms with CBOR Encoding";
   private static final String CBOR_REPORT = "CBOR Validation";
   private static final String JSON_REPORT = "JSON Validation";
   private static final String JSON_SCHEMA_CONSTRAINT = "DCC must be compliant with DGC 1.0.0 JSON Schema, data types and value sets";
   private CBORMapper cborMapper;

   /**
    * The specification dictates that we should tag date-time strings with 0, but during interoperability testing some validator apps have had
    * problems with this. Therefore, it is possible to turn off tagging.
    */
   private boolean tagDateTimesEnabled = true;

   public CBOREncoderDecoderV100() {
      SimpleModule timeModule = new JavaTimeModule();
      timeModule.addDeserializer(Instant.class, CBOREncoderDecoderV100.CustomInstantDeserializer.INSTANT);
      cborMapper = new CBORMapper();
      cborMapper.registerModule(timeModule);
      cborMapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
      cborMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
      cborMapper.setSerializationInclusion(JsonInclude.Include.NON_EMPTY);
   }

   @Override
   public EncodedCBORDCC encode(Eudgc content) throws EncodingException {
      try {
         return new EncodedCBORDCC(encodeInCBOR(content), null);
      } catch (JsonProcessingException e) {
         throw new EncodingException("Unable to encode DCC in CBOR", e);
      }
   }

   @Override
   public Eudgc decodeAndValidate(EncodedCBORDCC encodedContent) throws DecodingException, ValidationException {
      try {
         return cborMapper.readValue(encodedContent.getCborEncoded(), Eudgc.class);
      } catch (JsonMappingException e) {
         throw new ValidationException("CBOR content does not respect DCC JSON Schema", e);
      } catch (IOException e) {
         throw new DecodingException("Unable to decode CBOR object", e);
      }
   }

   @Override
   public Eudgc decodeAndReportValidation(final EncodedCBORDCC encodedContent, final ValidationReport report) throws DecodingException {
      ValidationSubReportBuilder cborReportBuilder = new ValidationSubReportBuilder(CBOR_REPORT);
      ValidationSubReportBuilder jsonReportBuilder = new ValidationSubReportBuilder(JSON_REPORT);
      Eudgc eudgc = null;
      try {
         eudgc = cborMapper.readValue(encodedContent.getCborEncoded(), Eudgc.class);
         cborReportBuilder.addPassedConstraintValidation(CBOR_CONSTRAINT, ConstraintPriority.MANDATORY);
         jsonReportBuilder.addPassedConstraintValidation(JSON_SCHEMA_CONSTRAINT, ConstraintPriority.MANDATORY);
      } catch (JsonMappingException e) {
         cborReportBuilder.addPassedConstraintValidation(CBOR_CONSTRAINT, ConstraintPriority.MANDATORY);
         jsonReportBuilder.addFailedConstraintValidation(JSON_SCHEMA_CONSTRAINT, ConstraintPriority.MANDATORY, e);
         throw new DecodingException("Unable to map decoded CBOR object on DGC Schema", e);
      } catch (IOException e) {
         cborReportBuilder.addFailedConstraintValidation(CBOR_CONSTRAINT, ConstraintPriority.MANDATORY, e);
         jsonReportBuilder.addUndefinedConstraintValidation(JSON_SCHEMA_CONSTRAINT, ConstraintPriority.MANDATORY);
         throw new DecodingException("Unable to decode CBOR object", e);
      } finally {
         report.addSubReport(cborReportBuilder.build());
         report.addSubReport(jsonReportBuilder.build());
      }
      return eudgc;
   }

   /**
    * Is tagging date-times enabled
    *
    * @return true if tagging date-time with 0 is enabled, false otherwise.
    */
   public boolean isTagDateTimesEnabled() {
      return tagDateTimesEnabled;
   }

   /**
    * The specification dictates that we should tag date-time strings with 0, but during interoperability testing some validator apps have had
    * problems with this. Therefore, it is possible to turn off tagging.
    * <p>
    * The default is to add the CBOR tag 0 for date-times.
    * </p>
    *
    * @param tagDateTimesEnabled true to tag date-times with 0, false otherwise.
    */
   public void setTagDateTimesEnabled(boolean tagDateTimesEnabled) {
      this.tagDateTimesEnabled = tagDateTimesEnabled;
   }

   /**
    * Encodes this object to its CBOR byte representation. Will tag date-time strings with 0 if tag-date-times is enabled.
    * <p>
    * COPIED from project se.digg, dgc-java
    *
    * @return the CBOR encoding
    *
    * @throws JsonProcessingException in case of error during JSON serialization and CBOR encoding.
    * @see https://github.com/ehn-digital-green-development/dgc-java
    */
   private byte[] encodeInCBOR(final Eudgc eudgc) throws JsonProcessingException {
      final byte[] encoding = cborMapper.writeValueAsBytes(eudgc);
      if (isTagDateTimesEnabled() && hasTestEntries(eudgc)) {
         final CBORObject cborDcc = CBORObject.DecodeFromBytes(encoding);
         final CBORObject testEntries = cborDcc.get("t");
         for (int i = 0; i < testEntries.size(); i++) {
            final CBORObject testEntry = testEntries.get(i);
            final CBORObject sampleCollectionInstant = testEntry.get("sc");
            if (sampleCollectionInstant != null && !sampleCollectionInstant.HasMostOuterTag(0)) {
               testEntry.set("sc", CBORObject.FromObjectAndTag(sampleCollectionInstant, 0));
            }
         }
         return cborDcc.EncodeToBytes();
      }
      return encoding;
   }

   private boolean hasTestEntries(Eudgc eudgc) {
      return eudgc.getT() != null && !eudgc.getT().isEmpty();
   }

   /**
    * Deserializer that can handle also ISO OFFSET.
    * <p>
    * COPIED from project se.digg, dgc-java
    *
    * @see https://github.com/ehn-digital-green-development/dgc-java
    */
   private static class CustomInstantDeserializer extends InstantDeserializer<Instant> {

      private static final long serialVersionUID = 3929100820024454525L;

      public static final CBOREncoderDecoderV100.CustomInstantDeserializer INSTANT = new CBOREncoderDecoderV100.CustomInstantDeserializer(
            Instant.class, DateTimeFormatter.ISO_OFFSET_DATE_TIME,
            Instant::from,
            a -> Instant.ofEpochMilli(a.value),
            a -> Instant.ofEpochSecond(a.integer, a.fraction),
            null,
            true);

      protected CustomInstantDeserializer(final Class<Instant> supportedType,
                                          final DateTimeFormatter formatter,
                                          final Function<TemporalAccessor, Instant> parsedToValue,
                                          final Function<FromIntegerArguments, Instant> fromMilliseconds,
                                          final Function<FromDecimalArguments, Instant> fromNanoseconds,
                                          final BiFunction<Instant, ZoneId, Instant> adjust,
                                          final boolean replaceZeroOffsetAsZ) {

         super(supportedType, formatter, parsedToValue, fromMilliseconds, fromNanoseconds, adjust, replaceZeroOffsetAsZ);
      }
   }
}
