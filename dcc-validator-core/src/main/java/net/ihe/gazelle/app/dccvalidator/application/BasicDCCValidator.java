package net.ihe.gazelle.app.dccvalidator.application;

import net.ihe.gazelle.app.dccvalidator.adapter.BarcodeEncoderDecoder;
import net.ihe.gazelle.app.dccvalidator.adapter.Base45EncoderDecoder;
import net.ihe.gazelle.app.dccvalidator.adapter.CBOREncoderDecoderV130;
import net.ihe.gazelle.app.dccvalidator.adapter.COSEEncoderDecoder;
import net.ihe.gazelle.app.dccvalidator.adapter.ZLibEncoderDecoder;
import net.ihe.gazelle.app.dccvalidator.business.BarcodeImageDCC;
import net.ihe.gazelle.app.dccvalidator.business.Concept;
import net.ihe.gazelle.app.dccvalidator.business.DCCValueSet;
import net.ihe.gazelle.app.dccvalidator.business.DecodingException;
import net.ihe.gazelle.app.dccvalidator.business.ValidationException;
import net.ihe.gazelle.modelapi.validation.business.InvalidObjectException;
import net.ihe.gazelle.modelapi.validation.business.ValidationOverview;
import net.ihe.gazelle.modelapi.validation.business.ValidationReport;

import java.security.cert.X509Certificate;
import java.util.List;
import java.util.Map;
import java.util.UUID;

public class BasicDCCValidator extends AbstractValidator {

   private final List<X509Certificate> trustedCertificates;
   private final Map<DCCValueSet, List<Concept>> dccValueSets;

   public BasicDCCValidator(List<X509Certificate> trustedCertificates, Map<DCCValueSet, List<Concept>> dccValueSets) {
      super("basic 1.0.0", "Basic DCC 1.0.0", "EU");
      this.trustedCertificates = trustedCertificates;
      this.dccValueSets = dccValueSets;
   }

   @Override
   public void validate(byte[] object) throws InvalidObjectException {
      try {
         new CBOREncoderDecoderV130(dccValueSets).decodeAndValidate(
               new COSEEncoderDecoder(null, null, trustedCertificates).decodeAndValidate(
                     new ZLibEncoderDecoder().decodeAndValidate(
                           new Base45EncoderDecoder().decodeAndValidate(
                                 new BarcodeEncoderDecoder().decodeAndValidate(new BarcodeImageDCC(object))
                           )
                     )
               )
         );
      } catch (DecodingException | ValidationException e) {
         throw new InvalidObjectException("The given Digital Covid Certificate is invalid", e);
      }
   }

   @Override
   public ValidationReport validateWithReport(byte[] object, ValidationOverview validationOverview) {
      final ValidationReport validationReport = new ValidationReport(UUID.randomUUID().toString(), validationOverview);
      try {
         new CBOREncoderDecoderV130(dccValueSets).decodeAndReportValidation(
               new COSEEncoderDecoder(null, null, trustedCertificates).decodeAndReportValidation(
                     new ZLibEncoderDecoder().decodeAndReportValidation(
                           new Base45EncoderDecoder().decodeAndReportValidation(
                                 new BarcodeEncoderDecoder().decodeAndReportValidation(new BarcodeImageDCC(object), validationReport),
                                 validationReport
                           ),
                           validationReport
                     ),
                     validationReport
               ),
               validationReport
         );
      } catch (DecodingException e) {
         // The excpetion is only used to interrupt the decoding process. It can be ignored because it is already logged in the validation report.
      }
      return validationReport;
   }
}
