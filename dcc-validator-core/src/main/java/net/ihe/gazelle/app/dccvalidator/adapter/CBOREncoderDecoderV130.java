package net.ihe.gazelle.app.dccvalidator.adapter;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.dataformat.cbor.databind.CBORMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.fasterxml.jackson.datatype.jsr310.deser.InstantDeserializer;
import com.upokecenter.cbor.CBORObject;
import net.ihe.gazelle.app.dccvalidator.business.Concept;
import net.ihe.gazelle.app.dccvalidator.business.DCCValueSet;
import net.ihe.gazelle.app.dccvalidator.business.DecodingException;
import net.ihe.gazelle.app.dccvalidator.business.EncodedCBORDCC;
import net.ihe.gazelle.app.dccvalidator.business.EncoderDecoder;
import net.ihe.gazelle.app.dccvalidator.business.EncodingException;
import net.ihe.gazelle.app.dccvalidator.business.ValidationException;
import net.ihe.gazelle.modelapi.dcc.v1_3_0.Eudcc;
import net.ihe.gazelle.modelapi.dcc.v1_3_0.RecoveryEntry;
import net.ihe.gazelle.modelapi.dcc.v1_3_0.TestEntry;
import net.ihe.gazelle.modelapi.dcc.v1_3_0.VaccinationEntry;
import net.ihe.gazelle.modelapi.validation.business.ConstraintPriority;
import net.ihe.gazelle.modelapi.validation.business.ValidationReport;

import java.io.IOException;
import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAccessor;
import java.util.List;
import java.util.Map;
import java.util.function.BiFunction;
import java.util.function.Function;

//FIXME Split into 2 EncoderDecoder : CBOR <-> JSON, and JSON <-> POJO
public class CBOREncoderDecoderV130 implements EncoderDecoder<EncodedCBORDCC, Eudcc> {

   private static final String CBOR_CONSTRAINT = "DCC must be conforms with CBOR Encoding";
   private static final String CBOR_REPORT = "CBOR Validation";
   private static final String JSON_REPORT = "JSON Validation";
   private static final String JSON_SCHEMA_CONSTRAINT = "DCC must be compliant with DGC 1.3.0 JSON Schema, data types and value sets";
   private static final String VALUE_CONSTRAINT_PASSED = "DCC must be compliant DGC 1.3.0 JSON Schema, data types and value sets : %s is in value set %s";
   private static final String VALUE_CONSTRAINT_FAILED = "DCC must be compliant DGC 1.3.0 JSON Schema, data types and value sets : %s is not in value set %s";
   private CBORMapper cborMapper;
   private Map<DCCValueSet, List<Concept>> dccValueSets;

   /**
    * The specification dictates that we should tag date-time strings with 0, but during interoperability testing some validator apps have had
    * problems with this. Therefore, it is possible to turn off tagging.
    */
   private boolean tagDateTimesEnabled = true;

   public CBOREncoderDecoderV130(Map<DCCValueSet, List<Concept>> dccValueSets) {
      SimpleModule timeModule = new JavaTimeModule();
      timeModule.addDeserializer(Instant.class, CBOREncoderDecoderV130.CustomInstantDeserializer.INSTANT);
      cborMapper = new CBORMapper();
      cborMapper.registerModule(timeModule);
      cborMapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
      cborMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
      cborMapper.setSerializationInclusion(JsonInclude.Include.NON_EMPTY);
      this.dccValueSets = dccValueSets;
   }

   @Override
   public EncodedCBORDCC encode(Eudcc content) throws EncodingException {
      try {
         return new EncodedCBORDCC(encodeInCBOR(content), null);
      } catch (JsonProcessingException e) {
         throw new EncodingException("Unable to encode DCC in CBOR", e);
      }
   }

   @Override
   public Eudcc decodeAndValidate(EncodedCBORDCC encodedContent) throws DecodingException, ValidationException {
      try {
         return cborMapper.readValue(encodedContent.getCborEncoded(), Eudcc.class);
      } catch (JsonMappingException e) {
         throw new ValidationException("CBOR content does not respect DCC JSON Schema", e);
      } catch (IOException e) {
         throw new DecodingException("Unable to decode CBOR object", e);
      }
   }

   @Override
   public Eudcc decodeAndReportValidation(final EncodedCBORDCC encodedContent, final ValidationReport report) throws DecodingException {
      ValidationSubReportBuilder cborReportBuilder = new ValidationSubReportBuilder(CBOR_REPORT);
      ValidationSubReportBuilder jsonReportBuilder = new ValidationSubReportBuilder(JSON_REPORT);
      Eudcc eudcc = null;
      try {
         eudcc = cborMapper.readValue(encodedContent.getCborEncoded(), Eudcc.class);
         cborReportBuilder.addPassedConstraintValidation(CBOR_CONSTRAINT, ConstraintPriority.MANDATORY);
         jsonReportBuilder.addPassedConstraintValidation(JSON_SCHEMA_CONSTRAINT, ConstraintPriority.MANDATORY);

         validateValueSets(eudcc, jsonReportBuilder);
      } catch (JsonMappingException e) {
         cborReportBuilder.addPassedConstraintValidation(CBOR_CONSTRAINT, ConstraintPriority.MANDATORY);
         jsonReportBuilder.addFailedConstraintValidation(JSON_SCHEMA_CONSTRAINT, ConstraintPriority.MANDATORY, e);
         throw new DecodingException("Unable to map decoded CBOR object on DGC Schema", e);
      } catch (IOException e) {
         cborReportBuilder.addFailedConstraintValidation(CBOR_CONSTRAINT, ConstraintPriority.MANDATORY, e);
         jsonReportBuilder.addUndefinedConstraintValidation(JSON_SCHEMA_CONSTRAINT, ConstraintPriority.MANDATORY);
         throw new DecodingException("Unable to decode CBOR object", e);
      } finally {
         report.addSubReport(cborReportBuilder.build());
         report.addSubReport(jsonReportBuilder.build());
      }
      return eudcc;
   }

   public void validateValueSets(Eudcc eudcc, ValidationSubReportBuilder jsonReportBuilder) {
      if(eudcc.getT() != null && !eudcc.getT().isEmpty()){
         for(TestEntry testEntry : eudcc.getT()){
            validateTg(testEntry.getTg(), jsonReportBuilder);
            validateCo(testEntry.getCo(), jsonReportBuilder);
            validateMaTest(testEntry.getMa(), jsonReportBuilder);
            validateTr(testEntry.getTr(), jsonReportBuilder);
            validateTt(testEntry.getTt(), jsonReportBuilder);
         }
      } else if(eudcc.getR() != null && !eudcc.getR().isEmpty()){
         for(RecoveryEntry recoveryEntry : eudcc.getR()){
            validateTg(recoveryEntry.getTg(), jsonReportBuilder);
            validateCo(recoveryEntry.getCo(), jsonReportBuilder);
         }
      } else if(eudcc.getV() != null && !eudcc.getV().isEmpty()){
         for(VaccinationEntry vaccinationEntry : eudcc.getV()){
            validateTg(vaccinationEntry.getTg(), jsonReportBuilder);
            validateVp(vaccinationEntry.getVp(), jsonReportBuilder);
            validateMp(vaccinationEntry.getMp(), jsonReportBuilder);
            validateMaVaccine(vaccinationEntry.getMa(), jsonReportBuilder);
            validateCo(vaccinationEntry.getCo(), jsonReportBuilder);

         }
      }

   }

   public void validateTg(String tg, ValidationSubReportBuilder jsonReportBuilder){
      if(isValueInValueSet(tg, dccValueSets.get(DCCValueSet.DISEASE_AGENT_TARGETED))){
         jsonReportBuilder.addPassedConstraintValidation(String.format(VALUE_CONSTRAINT_PASSED, tg, DCCValueSet.DISEASE_AGENT_TARGETED), ConstraintPriority.MANDATORY);
      } else {
         jsonReportBuilder.addFailedConstraintValidation(String.format(VALUE_CONSTRAINT_FAILED, tg, DCCValueSet.DISEASE_AGENT_TARGETED), ConstraintPriority.MANDATORY);
      }
   }

   public void validateVp(String vp, ValidationSubReportBuilder jsonReportBuilder){
      if(isValueInValueSet(vp, dccValueSets.get(DCCValueSet.VACCINE_PROPHYLAXIS))){
         jsonReportBuilder.addPassedConstraintValidation(String.format(VALUE_CONSTRAINT_PASSED, vp, DCCValueSet.VACCINE_PROPHYLAXIS), ConstraintPriority.MANDATORY);
      } else {
         jsonReportBuilder.addFailedConstraintValidation(String.format(VALUE_CONSTRAINT_FAILED, vp, DCCValueSet.VACCINE_PROPHYLAXIS), ConstraintPriority.MANDATORY);
      }
   }

   public void validateMp(String mp, ValidationSubReportBuilder jsonReportBuilder){
      if(isValueInValueSet(mp, dccValueSets.get(DCCValueSet.VACCINE_MEDICINAL_PRODUCT))){
         jsonReportBuilder.addPassedConstraintValidation(String.format(VALUE_CONSTRAINT_PASSED, mp, DCCValueSet.VACCINE_MEDICINAL_PRODUCT), ConstraintPriority.MANDATORY);
      } else {
         jsonReportBuilder.addFailedConstraintValidation(String.format(VALUE_CONSTRAINT_FAILED, mp, DCCValueSet.VACCINE_MEDICINAL_PRODUCT), ConstraintPriority.MANDATORY);
      }
   }

   public void validateMaVaccine(String ma, ValidationSubReportBuilder jsonReportBuilder){
      if(isValueInValueSet(ma, dccValueSets.get(DCCValueSet.VACCINE_MAH_MANF))){
         jsonReportBuilder.addPassedConstraintValidation(String.format(VALUE_CONSTRAINT_PASSED, ma, DCCValueSet.VACCINE_MAH_MANF), ConstraintPriority.MANDATORY);
      } else {
         jsonReportBuilder.addFailedConstraintValidation(String.format(VALUE_CONSTRAINT_FAILED, ma, DCCValueSet.VACCINE_MAH_MANF), ConstraintPriority.MANDATORY);
      }
   }

   public void validateMaTest(String ma, ValidationSubReportBuilder jsonReportBuilder){
      if(isValueInValueSet(ma, dccValueSets.get(DCCValueSet.TEST_MANF))){
         jsonReportBuilder.addPassedConstraintValidation(String.format(VALUE_CONSTRAINT_PASSED, ma, DCCValueSet.TEST_MANF), ConstraintPriority.MANDATORY);
      } else {
         jsonReportBuilder.addFailedConstraintValidation(String.format(VALUE_CONSTRAINT_FAILED, ma, DCCValueSet.TEST_MANF), ConstraintPriority.MANDATORY);
      }
   }

   public void validateCo(String co, ValidationSubReportBuilder jsonReportBuilder){
      if(isValueInValueSet(co, dccValueSets.get(DCCValueSet.COUNTRY_2_CODE))){
         jsonReportBuilder.addPassedConstraintValidation(String.format(VALUE_CONSTRAINT_PASSED, co, DCCValueSet.COUNTRY_2_CODE), ConstraintPriority.MANDATORY);
      } else {
         jsonReportBuilder.addFailedConstraintValidation(String.format(VALUE_CONSTRAINT_FAILED, co, DCCValueSet.COUNTRY_2_CODE), ConstraintPriority.MANDATORY);
      }
   }

   public void validateTr(String tr, ValidationSubReportBuilder jsonReportBuilder){
      if(isValueInValueSet(tr, dccValueSets.get(DCCValueSet.TEST_RESULT))){
         jsonReportBuilder.addPassedConstraintValidation(String.format(VALUE_CONSTRAINT_PASSED, tr, DCCValueSet.TEST_RESULT), ConstraintPriority.MANDATORY);
      } else {
         jsonReportBuilder.addFailedConstraintValidation(String.format(VALUE_CONSTRAINT_FAILED, tr, DCCValueSet.TEST_RESULT), ConstraintPriority.MANDATORY);
      }
   }

   public void validateTt(String tt, ValidationSubReportBuilder jsonReportBuilder){
      if(isValueInValueSet(tt, dccValueSets.get(DCCValueSet.TEST_TYPE))){
         jsonReportBuilder.addPassedConstraintValidation(String.format(VALUE_CONSTRAINT_PASSED, tt, DCCValueSet.TEST_TYPE), ConstraintPriority.MANDATORY);
      } else {
         jsonReportBuilder.addFailedConstraintValidation(String.format(VALUE_CONSTRAINT_FAILED, tt, DCCValueSet.TEST_TYPE), ConstraintPriority.MANDATORY);
      }
   }

   public boolean isValueInValueSet(String value, List<Concept> valueSet){
      if(valueSet != null) {
         for (Concept concept : valueSet) {
            if (concept.getCode().equals(value)) {
               return true;
            }
         }
      }
      return false;
   }

   /**
    * Is tagging date-times enabled
    *
    * @return true if tagging date-time with 0 is enabled, false otherwise.
    */
   public boolean isTagDateTimesEnabled() {
      return tagDateTimesEnabled;
   }

   /**
    * The specification dictates that we should tag date-time strings with 0, but during interoperability testing some validator apps have had
    * problems with this. Therefore, it is possible to turn off tagging.
    * <p>
    * The default is to add the CBOR tag 0 for date-times.
    * </p>
    *
    * @param tagDateTimesEnabled true to tag date-times with 0, false otherwise.
    */
   public void setTagDateTimesEnabled(boolean tagDateTimesEnabled) {
      this.tagDateTimesEnabled = tagDateTimesEnabled;
   }

   /**
    * Encodes this object to its CBOR byte representation. Will tag date-time strings with 0 if tag-date-times is enabled.
    * <p>
    * COPIED from project se.digg, dgc-java
    *
    * @return the CBOR encoding
    *
    * @throws JsonProcessingException in case of error during JSON serialization and CBOR encoding.
    * @see <a href="https://github.com/ehn-digital-green-development/dgc-java">https://github.com/ehn-digital-green-development/dgc-java</a>
    */
   private byte[] encodeInCBOR(final Eudcc eudcc) throws JsonProcessingException {
      final byte[] encoding = cborMapper.writeValueAsBytes(eudcc);
      if (isTagDateTimesEnabled() && hasTestEntries(eudcc)) {
         final CBORObject cborDcc = CBORObject.DecodeFromBytes(encoding);
         final CBORObject testEntries = cborDcc.get("t");
         for (int i = 0; i < testEntries.size(); i++) {
            final CBORObject testEntry = testEntries.get(i);
            final CBORObject sampleCollectionInstant = testEntry.get("sc");
            if (sampleCollectionInstant != null && !sampleCollectionInstant.HasMostOuterTag(0)) {
               testEntry.set("sc", CBORObject.FromObjectAndTag(sampleCollectionInstant, 0));
            }
         }
         return cborDcc.EncodeToBytes();
      }
      return encoding;
   }

   private boolean hasTestEntries(Eudcc eudgc) {
      return eudgc.getT() != null && !eudgc.getT().isEmpty();
   }

   /**
    * Deserializer that can handle also ISO OFFSET.
    * <p>
    * COPIED from project se.digg, dgc-java
    *
    * @see <a href="https://github.com/ehn-digital-green-development/dgc-java">https://github.com/ehn-digital-green-development/dgc-java</a>
    */
   private static class CustomInstantDeserializer extends InstantDeserializer<Instant> {

      private static final long serialVersionUID = 3929100820024454525L;

      public static final CBOREncoderDecoderV130.CustomInstantDeserializer INSTANT = new CBOREncoderDecoderV130.CustomInstantDeserializer(
            Instant.class, DateTimeFormatter.ISO_OFFSET_DATE_TIME,
            Instant::from,
            a -> Instant.ofEpochMilli(a.value),
            a -> Instant.ofEpochSecond(a.integer, a.fraction),
            null,
            true);

      protected CustomInstantDeserializer(final Class<Instant> supportedType,
                                          final DateTimeFormatter formatter,
                                          final Function<TemporalAccessor, Instant> parsedToValue,
                                          final Function<FromIntegerArguments, Instant> fromMilliseconds,
                                          final Function<FromDecimalArguments, Instant> fromNanoseconds,
                                          final BiFunction<Instant, ZoneId, Instant> adjust,
                                          final boolean replaceZeroOffsetAsZ) {

         super(supportedType, formatter, parsedToValue, fromMilliseconds, fromNanoseconds, adjust, replaceZeroOffsetAsZ);
      }
   }
}
