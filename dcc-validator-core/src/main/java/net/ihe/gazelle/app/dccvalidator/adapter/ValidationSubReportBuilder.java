package net.ihe.gazelle.app.dccvalidator.adapter;

import net.ihe.gazelle.modelapi.validation.business.ConstraintPriority;
import net.ihe.gazelle.modelapi.validation.business.ConstraintValidation;
import net.ihe.gazelle.modelapi.validation.business.ValidationSubReport;
import net.ihe.gazelle.modelapi.validation.business.ValidationTestResult;

import java.util.ArrayList;
import java.util.List;

/**
 * ValidationReport helper class to build a ValidationReport with java 8 style.
 */
public class ValidationSubReportBuilder {

   private String subReportName;
   private List<ConstraintValidation> constraints = new ArrayList<>();

   ValidationSubReportBuilder(String subReportName) {
      this.subReportName = subReportName;
   }

   public ValidationSubReportBuilder addPassedConstraintValidation(String constraintDescription,
                                                                   ConstraintPriority constraintPriority) {
      return addConstraintValidation(constraintDescription, constraintPriority, ValidationTestResult.PASSED);
   }

   public ValidationSubReportBuilder addFailedConstraintValidation(String constraintDescription,
                                                                   ConstraintPriority constraintPriority) {
      return addConstraintValidation(constraintDescription, constraintPriority, ValidationTestResult.FAILED);
   }

   public ValidationSubReportBuilder addFailedConstraintValidation(String constraintDescription,
                                                                   ConstraintPriority constraintPriority, Throwable cause) {
      return addConstraintValidation(constraintDescription,
            constraintPriority, ValidationTestResult.FAILED, cause);
   }

   public ValidationSubReportBuilder addUndefinedConstraintValidation(String constraintDescription,
                                                                      ConstraintPriority constraintPriority) {
      return addConstraintValidation(constraintDescription, constraintPriority, ValidationTestResult.UNDEFINED);
   }

   public ValidationSubReportBuilder addUndefinedConstraintValidation(String constraintDescription,
                                                                      ConstraintPriority constraintPriority, Throwable cause) {
      return addConstraintValidation(constraintDescription, constraintPriority, ValidationTestResult.UNDEFINED, cause);
   }

   public ValidationSubReport build() {
      ValidationSubReport subReport = new ValidationSubReport(subReportName, null);
      constraints.forEach(subReport::addConstraintValidation);
      return subReport;
   }

   private ValidationSubReportBuilder addConstraintValidation(String constraintDescription,
                                                              ConstraintPriority constraintPriority, ValidationTestResult testResult,
                                                              Throwable cause) {
      //FIXME exception should be added to the dedicated field ValidationSubReport.exceptions or ConstraintValidation.exceptions instead of
      // concatenated in the description
      return addConstraintValidation(constraintDescription + System.lineSeparator() + flattenExceptionCauseMessages(cause),
              constraintPriority, testResult);
   }

   private ValidationSubReportBuilder addConstraintValidation(String constraintDescription,
                                                              ConstraintPriority constraintPriority, ValidationTestResult testResult) {
      constraints.add(new ConstraintValidation(constraintDescription, constraintPriority, testResult));
      return this;
   }

   private String flattenExceptionCauseMessages(Throwable cause) {
      return cause.getCause() != null ?
            cause.getMessage() + System.lineSeparator() + flattenExceptionCauseMessages(cause.getCause()) :
            cause.getMessage();
   }

}
