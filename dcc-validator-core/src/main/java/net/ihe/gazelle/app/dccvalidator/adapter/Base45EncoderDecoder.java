package net.ihe.gazelle.app.dccvalidator.adapter;

import net.ihe.gazelle.app.dccvalidator.business.CompressedZLibDCC;
import net.ihe.gazelle.app.dccvalidator.business.DecodingException;
import net.ihe.gazelle.app.dccvalidator.business.EncodedBase45DCC;
import net.ihe.gazelle.app.dccvalidator.business.EncoderDecoder;
import net.ihe.gazelle.app.dccvalidator.business.EncodingException;
import net.ihe.gazelle.app.dccvalidator.business.ValidationException;
import net.ihe.gazelle.modelapi.validation.business.ConstraintPriority;
import net.ihe.gazelle.modelapi.validation.business.ValidationReport;
import se.digg.dgc.encoding.Base45;

public class Base45EncoderDecoder implements EncoderDecoder<EncodedBase45DCC, CompressedZLibDCC> {

   private static final String BASE45_REPORT = "Base45 Encoding";
   private static final String BASE45_CONSTRAINT = "DCC Compressed must be encoded using Base45.";

   @Override
   public EncodedBase45DCC encode(CompressedZLibDCC content) throws EncodingException {
      return new EncodedBase45DCC(Base45.getEncoder().encodeToString(content.getContent()));
   }

   @Override
   public CompressedZLibDCC decodeAndValidate(EncodedBase45DCC encodedContent) throws DecodingException, ValidationException {
      try{
         return new CompressedZLibDCC(Base45.getDecoder().decode(encodedContent.getBase45string()));
      } catch (IllegalArgumentException e){
         throw new DecodingException("Unable to decode the given Base45", e);
      }
   }

   @Override
   public CompressedZLibDCC decodeAndReportValidation(EncodedBase45DCC encodedContent, final ValidationReport report) throws DecodingException {
      ValidationSubReportBuilder base45ReportBuilder = new ValidationSubReportBuilder(BASE45_REPORT);
      CompressedZLibDCC compressedZLibDCC = null;
      try{
         compressedZLibDCC = new CompressedZLibDCC(Base45.getDecoder().decode(encodedContent.getBase45string()));
         base45ReportBuilder.addPassedConstraintValidation(BASE45_CONSTRAINT, ConstraintPriority.MANDATORY);
      } catch (IllegalArgumentException e){
         base45ReportBuilder.addFailedConstraintValidation(BASE45_CONSTRAINT, ConstraintPriority.MANDATORY, e);
         throw new DecodingException("Unable to decode the given Base45", e);
      } finally {
         report.addSubReport(base45ReportBuilder.build());
      }
      return compressedZLibDCC;
   }
}
