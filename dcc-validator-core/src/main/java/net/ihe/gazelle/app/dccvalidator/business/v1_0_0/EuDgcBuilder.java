package net.ihe.gazelle.app.dccvalidator.business.v1_0_0;

import net.ihe.gazelle.modelapi.dcc.v1_0_0.Eudgc;
import net.ihe.gazelle.modelapi.dcc.v1_0_0.PersonName;
import se.digg.dgc.transliteration.MrzEncoder;

import java.time.LocalDate;

public class EuDgcBuilder {

   private static final String DCC_SCHEMA_VERSION = "1.0.0";

   private Eudgc eudcc = new Eudgc();

   public EuDgcBuilder() {
      eudcc.setVer(DCC_SCHEMA_VERSION);
   }

   public EuDgcBuilder setPersonName(String familyName, String givenName) {
      //FIXME call transliterator MrzEncoder through an interface
      eudcc.setNam(new PersonName(familyName, MrzEncoder.encode(familyName), givenName, MrzEncoder.encode(givenName)));
      return this;
   }

   public EuDgcBuilder setDateOfBirth(Integer year, Integer month, Integer day) {
      eudcc.setDob(LocalDate.of(year, month, day));
      return this;
   }

   public Eudgc build() {
      return eudcc;
   }

}
