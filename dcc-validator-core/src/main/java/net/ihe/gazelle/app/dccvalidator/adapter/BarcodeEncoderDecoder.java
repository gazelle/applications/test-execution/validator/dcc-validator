package net.ihe.gazelle.app.dccvalidator.adapter;

import net.ihe.gazelle.app.dccvalidator.business.BarcodeImageDCC;
import net.ihe.gazelle.app.dccvalidator.business.DecodingException;
import net.ihe.gazelle.app.dccvalidator.business.EncodedBase45DCC;
import net.ihe.gazelle.app.dccvalidator.business.EncoderDecoder;
import net.ihe.gazelle.app.dccvalidator.business.EncodingException;
import net.ihe.gazelle.app.dccvalidator.business.ValidationException;
import net.ihe.gazelle.modelapi.validation.business.ConstraintPriority;
import net.ihe.gazelle.modelapi.validation.business.ValidationReport;
import se.digg.dgc.encoding.Barcode;
import se.digg.dgc.encoding.BarcodeDecoder;
import se.digg.dgc.encoding.BarcodeException;
import se.digg.dgc.encoding.impl.DefaultBarcodeCreator;
import se.digg.dgc.encoding.impl.DefaultBarcodeDecoder;

import java.nio.charset.StandardCharsets;

public class BarcodeEncoderDecoder implements EncoderDecoder<BarcodeImageDCC, EncodedBase45DCC> {

   public static final String DCC_V1_PREFIX = "HC1:";
   private static final String BARCODE_REPORT = "QR Barcode Validation";
   private static final String QR_CONSTRAINT = "DCC should be encoded as a QR code";
   private static final String HC1_PREFIX_CONSTRAINT = "DCC Barcode content must be prefixed with 'HC1:' prefix.";

   @Override
   public BarcodeImageDCC encode(final EncodedBase45DCC content) throws EncodingException {
      try {
         return new BarcodeImageDCC(new DefaultBarcodeCreator().create(DCC_V1_PREFIX + content.getBase45string()).getImage());
      } catch (BarcodeException e) {
         throw new EncodingException("Unable to encode Barcode from given Base45 content", e);
      }
   }

   @Override
   public EncodedBase45DCC decodeAndValidate(final BarcodeImageDCC encodedContent) throws DecodingException, ValidationException {
      String decodedBarcode = decodeBarcode(encodedContent.getImageBytes(), encodedContent.getBarcodeType().name());
      return getBase45AfterPrefix(decodedBarcode);
   }



   @Override
   public EncodedBase45DCC decodeAndReportValidation(final BarcodeImageDCC encodedContent, final ValidationReport report) throws DecodingException {
      ValidationSubReportBuilder barcodeReportBuilder = new ValidationSubReportBuilder(BARCODE_REPORT);
      String decodedBarcode = null ;
      try {
         decodedBarcode = decodeBarcode(encodedContent.getImageBytes(), encodedContent.getBarcodeType().name());
         barcodeReportBuilder.addPassedConstraintValidation(QR_CONSTRAINT, ConstraintPriority.RECOMMENDED);
      } catch (DecodingException e) {
         barcodeReportBuilder.addFailedConstraintValidation(QR_CONSTRAINT, ConstraintPriority.RECOMMENDED, e);
         report.addSubReport(barcodeReportBuilder.build());
         throw e;
      }

      EncodedBase45DCC base45DCC = null;
      try {
         base45DCC = getBase45AfterPrefix(decodedBarcode);
         barcodeReportBuilder.addPassedConstraintValidation(HC1_PREFIX_CONSTRAINT, ConstraintPriority.MANDATORY);
      } catch (ValidationException e) {
         base45DCC = new EncodedBase45DCC(decodedBarcode);
         barcodeReportBuilder.addFailedConstraintValidation(HC1_PREFIX_CONSTRAINT, ConstraintPriority.MANDATORY, e);
      }
      report.addSubReport(barcodeReportBuilder.build());
      return base45DCC;
   }

   private String decodeBarcode(byte[] barcode, String barcodeType) throws DecodingException {
      BarcodeDecoder decoder = new DefaultBarcodeDecoder();
      try {
         return decoder.decodeToString(barcode, Barcode.BarcodeType.parse(barcodeType), StandardCharsets.UTF_8);
      } catch (BarcodeException e) {
         throw new DecodingException("Unable to decode the given barcode.", e);
      }
   }

   //FIXME the prefix controle should be in application or business layer.
   private EncodedBase45DCC getBase45AfterPrefix(String decodedBarcode) throws ValidationException {
      if (decodedBarcode.startsWith(DCC_V1_PREFIX)) {
         return new EncodedBase45DCC(decodedBarcode.substring(DCC_V1_PREFIX.length()));
      } else {
         throw new ValidationException(String.format("Missing DCC prefix '%s' before the encoded Base45", DCC_V1_PREFIX));
      }
   }
}
