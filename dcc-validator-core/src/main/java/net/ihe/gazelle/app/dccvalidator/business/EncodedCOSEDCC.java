package net.ihe.gazelle.app.dccvalidator.business;

import java.util.Arrays;

public class EncodedCOSEDCC {

   private byte[] encodedCOSE ;

   public EncodedCOSEDCC(byte[] encodedCOSE) {
      setEncodedCOSE(encodedCOSE);
   }

   public byte[] getEncodedCOSE() {
      return Arrays.copyOf(encodedCOSE, encodedCOSE.length);
   }

   public void setEncodedCOSE(byte[] encodedCOSE) {
      this.encodedCOSE = Arrays.copyOf(encodedCOSE, encodedCOSE.length);
   }

}
