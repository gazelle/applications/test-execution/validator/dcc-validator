package net.ihe.gazelle.app.dccvalidator.application;

import net.ihe.gazelle.modelapi.validation.business.InvalidObjectException;
import net.ihe.gazelle.modelapi.validation.business.ValidationOverview;
import net.ihe.gazelle.modelapi.validation.business.ValidationReport;
import net.ihe.gazelle.modelapi.validation.business.Validator;

public abstract class AbstractValidator extends Validator {

   public AbstractValidator(String keyword) {
      super(keyword);
   }

   public AbstractValidator(String keyword, String name, String domain) {
      super(keyword, name, domain);
   }

   public abstract void validate(byte[] object) throws InvalidObjectException;

   public abstract ValidationReport validateWithReport(byte[] object, ValidationOverview validationOverview);

}
