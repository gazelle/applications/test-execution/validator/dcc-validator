package net.ihe.gazelle.app.dccvalidator.business;

import java.time.Instant;
import java.util.Arrays;

public class EncodedCBORDCC {

   private Instant expiration;

   private byte[] cborEncoded;

   public EncodedCBORDCC(byte[] cborEncoded, Instant expiration) {
      this.cborEncoded = Arrays.copyOf(cborEncoded, cborEncoded.length);
      this.expiration = expiration;
   }

   public byte[] getCborEncoded() {
      return Arrays.copyOf(cborEncoded, cborEncoded.length);
   }

   public void setCborEncoded(byte[] cborEncoded) {
      this.cborEncoded = Arrays.copyOf(cborEncoded, cborEncoded.length);
   }

   public Instant getExpiration() {
      return expiration;
   }

   public void setExpiration(Instant expiration) {
      this.expiration = expiration;
   }
}
