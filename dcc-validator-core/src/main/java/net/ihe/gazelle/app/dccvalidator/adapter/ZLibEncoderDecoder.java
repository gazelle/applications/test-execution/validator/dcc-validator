package net.ihe.gazelle.app.dccvalidator.adapter;

import net.ihe.gazelle.app.dccvalidator.business.CompressedZLibDCC;
import net.ihe.gazelle.app.dccvalidator.business.DecodingException;
import net.ihe.gazelle.app.dccvalidator.business.EncodedCOSEDCC;
import net.ihe.gazelle.app.dccvalidator.business.EncoderDecoder;
import net.ihe.gazelle.app.dccvalidator.business.EncodingException;
import net.ihe.gazelle.app.dccvalidator.business.ValidationException;
import net.ihe.gazelle.modelapi.validation.business.ConstraintPriority;
import net.ihe.gazelle.modelapi.validation.business.ValidationReport;
import se.digg.dgc.encoding.Zlib;

import java.util.zip.ZipException;

public class ZLibEncoderDecoder implements EncoderDecoder<CompressedZLibDCC, EncodedCOSEDCC> {

   private static final String ZLIB_REPORT = "ZLib Compression";
   private static final String ZLIB_CONSTRAINT = "DCC must be compressed with Deflate ZLib";

   @Override
   public CompressedZLibDCC encode(EncodedCOSEDCC content) throws EncodingException {
      return new CompressedZLibDCC(Zlib.compress(content.getEncodedCOSE()));
   }

   @Override
   public EncodedCOSEDCC decodeAndValidate(CompressedZLibDCC encodedContent) throws DecodingException, ValidationException {
      try {
         return new EncodedCOSEDCC(Zlib.decompress(encodedContent.getContent(), true));
      } catch (ZipException e) {
         throw new DecodingException("Unable to inflate zlib data", e);
      }
   }

   @Override
   public EncodedCOSEDCC decodeAndReportValidation(CompressedZLibDCC encodedContent, final ValidationReport report) throws DecodingException {
      ValidationSubReportBuilder zlibReportBuilder = new ValidationSubReportBuilder(ZLIB_REPORT);
      EncodedCOSEDCC cosedcc = null;
      try {
         cosedcc = new EncodedCOSEDCC(Zlib.decompress(encodedContent.getContent(), true));
         zlibReportBuilder.addPassedConstraintValidation(ZLIB_CONSTRAINT, ConstraintPriority.MANDATORY);
      } catch (ZipException e) {
         zlibReportBuilder.addFailedConstraintValidation(ZLIB_CONSTRAINT, ConstraintPriority.MANDATORY, e);
         throw new DecodingException("Unable to inflate zlib data", e);
      } finally {
         report.addSubReport(zlibReportBuilder.build());
      }
      return cosedcc;
   }
}
