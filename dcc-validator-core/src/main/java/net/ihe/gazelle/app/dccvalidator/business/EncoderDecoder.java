package net.ihe.gazelle.app.dccvalidator.business;

import net.ihe.gazelle.modelapi.validation.business.ValidationReport;

public interface EncoderDecoder<E, C> {

   E encode(final C content) throws EncodingException;
   C decodeAndValidate(final E encodedContent) throws DecodingException, ValidationException;
   C decodeAndReportValidation(final E encodedContent, final ValidationReport report) throws DecodingException;

}
