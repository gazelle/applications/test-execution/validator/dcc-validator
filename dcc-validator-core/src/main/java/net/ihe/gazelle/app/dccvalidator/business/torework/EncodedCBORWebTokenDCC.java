package net.ihe.gazelle.app.dccvalidator.business.torework;

public class EncodedCBORWebTokenDCC {

   private byte[] content;

   public EncodedCBORWebTokenDCC(byte[] content) {
      this.content = content;
   }

   public byte[] getContent() {
      return content;
   }

   public void setContent(byte[] content) {
      if(content != null) {
         this.content = content;
      } else {
         throw new IllegalArgumentException("Content for DCC Compressed CWT must not be null");
      }
   }
}
