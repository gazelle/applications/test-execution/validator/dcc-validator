package net.ihe.gazelle.app.dccvalidator.business;

public enum DCCValueSet {
    COUNTRY_2_CODE("country-2-codes.json", "1.1.1.1.1.1"),
    DISEASE_AGENT_TARGETED("disease-agent-targeted.json", "1.1.1.1.1.2"),
    TEST_MANF("test-manf.json", "1.1.1.1.1.1.3"),
    TEST_RESULT("test-result.json", "1.1.1.1.1.4"),
    TEST_TYPE("test-type.json", "1.1.1.1.1.5"),
    VACCINE_MAH_MANF("vaccine-mah-manf.json", "1.1.1.1.1.6"),
    VACCINE_MEDICINAL_PRODUCT("vaccine-medicinal-product.json", "1.1.1.1.1.7"),
    VACCINE_PROPHYLAXIS("vaccine-prophylaxis.json", "1.1.1.1.1.8");

    public String fileName;
    public String oid;

    DCCValueSet(String fileName, String oid) {
        this.fileName = fileName;
        this.oid = oid;
    }
}
