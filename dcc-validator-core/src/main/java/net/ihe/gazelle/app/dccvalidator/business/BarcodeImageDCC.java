package net.ihe.gazelle.app.dccvalidator.business;

import java.util.Arrays;

public class BarcodeImageDCC {

   public static final ImageType DEFAULT_IMAGE_TYPE = ImageType.PNG;
   public static final BarcodeType DEFAULT_BARCODE_TYPE = BarcodeType.QR;

   private final ImageType imageType;
   private final BarcodeType barcodeType;
   private final byte[] imageBytes;

   public BarcodeImageDCC(byte[] imageBytes) {
      if(imageBytes != null) {
         this.imageBytes = Arrays.copyOf(imageBytes, imageBytes.length);
         imageType = DEFAULT_IMAGE_TYPE;
         barcodeType = DEFAULT_BARCODE_TYPE;
      } else {
         throw new IllegalArgumentException("Image bytes must not be null");
      }
   }

   public byte[] getImageBytes() {
      return Arrays.copyOf(imageBytes, imageBytes.length);
   }

   public ImageType getImageType() {
      return imageType;
   }

   public BarcodeType getBarcodeType() {
      return barcodeType;
   }

   public enum ImageType {
      PNG
   }

   public enum BarcodeType {
      QR
   }
}
