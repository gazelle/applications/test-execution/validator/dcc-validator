package net.ihe.gazelle.app.dccvalidator.adapter;

import net.ihe.gazelle.app.dccvalidator.business.DecodingException;
import net.ihe.gazelle.app.dccvalidator.business.EncodedCBORDCC;
import net.ihe.gazelle.app.dccvalidator.business.EncodingException;
import net.ihe.gazelle.app.dccvalidator.business.ValidationException;
import net.ihe.gazelle.app.dccvalidator.business.v1_0_0.EuDgcBuilder;
import net.ihe.gazelle.app.dccvalidator.business.v1_3_0.EuDccBuilder;
import net.ihe.gazelle.modelapi.dcc.v1_0_0.Eudgc;
import net.ihe.gazelle.modelapi.dcc.v1_0_0.TestEntry;
import net.ihe.gazelle.modelapi.dcc.v1_3_0.Eudcc;
import net.ihe.gazelle.modelapi.validation.business.ValidationReport;
import net.ihe.gazelle.modelapi.validation.business.ValidationTestResult;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.opentest4j.AssertionFailedError;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.time.Instant;
import java.util.List;

public class CBOREncoderDecoderV100Test {

   private final ValidationReport4TestUtil validationReport4TestUtil = new ValidationReport4TestUtil();
   private Eudgc testResourceDGC;
   private byte[] testResourceCBORDGC;
   private CBOREncoderDecoderV100 codec;

   @BeforeEach
   public void setUp() throws IOException {
      testResourceDGC = new EuDgcBuilder().setPersonName("Forgeron", "Jean").setDateOfBirth(1978, 01, 01).build();
      testResourceCBORDGC = getResourceAsStream("dgc-test-1.0.0.cbor").readAllBytes();
      codec = new CBOREncoderDecoderV100();
   }

   @Test
   public void simpleEncodeTest() throws EncodingException, IOException {
      EncodedCBORDCC encoded = codec.encode(testResourceDGC);
      Assertions.assertArrayEquals(testResourceCBORDGC, encoded.getCborEncoded());
   }

   @Test
   public void simpleDecodeTest() throws IOException, ValidationException, DecodingException {
      Eudgc decodedDGC = codec.decodeAndValidate(new EncodedCBORDCC(testResourceCBORDGC, null));
      Assertions.assertEquals(testResourceDGC, decodedDGC);
   }

   @Test
   public void simpleDecodeWithReportTest() throws DecodingException {
      final ValidationReport report = validationReport4TestUtil.createValidationReportForTest(this.getClass());
      Eudgc decodedDGC = codec.decodeAndReportValidation(new EncodedCBORDCC(testResourceCBORDGC, null), report);
      Assertions.assertEquals(ValidationTestResult.PASSED, report.getValidationOverallResult());
      Assertions.assertNotNull(report.getSubReports().stream().filter(sr -> sr.getName().equals("CBOR Validation")).findFirst().orElseThrow(
            AssertionFailedError::new));
      Assertions.assertNotNull(report.getSubReports().stream().filter(sr -> sr.getName().equals("JSON Validation")).findFirst().orElseThrow(
            AssertionFailedError::new));
   }

   @Test
   public void decodeCborWithReportErrorTest() {
      final ValidationReport report = validationReport4TestUtil.createValidationReportForTest(this.getClass());

      Assertions.assertThrows(DecodingException.class,
            () -> codec.decodeAndReportValidation(new EncodedCBORDCC("This is not a CBOR".getBytes(StandardCharsets.UTF_8), null), report));

      assertReport(report, ValidationTestResult.UNDEFINED, ValidationTestResult.FAILED, ValidationTestResult.UNDEFINED);
   }

   @Test
   public void jsonSchemaWithReportErrorTest() throws EncodingException {

      // Build a DCC version 1.3.0 to trigger an error while checking for 1.0.0 schema
      Eudcc testResourceDCCV130 = new EuDccBuilder().setPersonName("Forgeron", "Jean").setDateOfBirth(1978, 01, 01).build();
      net.ihe.gazelle.modelapi.dcc.v1_3_0.TestEntry testEntry = new net.ihe.gazelle.modelapi.dcc.v1_3_0.TestEntry("840539006", "LP217198-3", null,
            "2108", Instant.now(), "260415000", "CG Center", "FR",
            "https://gazelle.ihe.net/Fakelab4test", "URN:UVCI:01:FR:123/123456789");
      testResourceDCCV130.setT(List.of(testEntry));
      EncodedCBORDCC encodedCBORDCCV130 = new CBOREncoderDecoderV130(ValueSetProviderForUT.getDccValueSets()).encode(testResourceDCCV130);

      final ValidationReport report = validationReport4TestUtil.createValidationReportForTest(this.getClass());

      Assertions.assertThrows(DecodingException.class, () -> codec.decodeAndReportValidation(encodedCBORDCCV130, report));
      assertReport(report, ValidationTestResult.FAILED, ValidationTestResult.PASSED, ValidationTestResult.FAILED);
   }

   @Test
   @Disabled
   //FIXME Value Sets are not verified !
   public void valueSetsWithReportErrorTest() throws EncodingException {
      // Build a DCC version 1.0.0 with wrong tg value sets (COVID-19 instead of 840539006) to trigger an error
      TestEntry testEntry = new TestEntry("COVID-19", "LP217198-3", null, "2108", Instant.now(), Instant.now(), "260415000", "CG Center", "FR",
            "https://gazelle.ihe.net/Fakelab4test", "URN:UVCI:01:FR:123/123456789");
      testResourceDGC.setT(List.of(testEntry));
      EncodedCBORDCC encodedCBORDCC = codec.encode(testResourceDGC);

      final ValidationReport report = validationReport4TestUtil.createValidationReportForTest(this.getClass());

      Assertions.assertThrows(DecodingException.class, () -> codec.decodeAndReportValidation(encodedCBORDCC, report));
      assertReport(report, ValidationTestResult.FAILED, ValidationTestResult.PASSED, ValidationTestResult.FAILED);
   }

   private void assertReport(ValidationReport report, ValidationTestResult reportResult, ValidationTestResult cborConstraintResult, ValidationTestResult jsonConstraintResult) {
      Assertions.assertEquals(reportResult, report.getValidationOverallResult());
      Assertions.assertEquals(cborConstraintResult,
            report.getSubReports().stream().filter(sr -> sr.getName().equals("CBOR Validation")).findFirst()
                  .orElseThrow(AssertionFailedError::new)
                  .getConstraints().stream().filter(c -> c.getConstraintDescription().contains("CBOR Encoding")).findFirst()
                  .orElseThrow(AssertionFailedError::new)
                  .getTestResult());
      Assertions.assertEquals(jsonConstraintResult,
            report.getSubReports().stream().filter(sr -> sr.getName().equals("JSON Validation")).findFirst()
                  .orElseThrow(AssertionFailedError::new)
                  .getConstraints().stream().filter(c -> c.getConstraintDescription().contains("JSON Schema")).findFirst()
                  .orElseThrow(AssertionFailedError::new)
                  .getTestResult());
   }

   private static InputStream getResourceAsStream(String ressourcePath) {
      return CBOREncoderDecoderV100Test.class.getClassLoader().getResourceAsStream(ressourcePath);
   }

}
