package net.ihe.gazelle.app.dccvalidator.adapter;

import com.upokecenter.cbor.CBORObject;
import net.ihe.gazelle.app.dccvalidator.application.CryptoMaterial4TestLoader;
import net.ihe.gazelle.app.dccvalidator.business.DecodingException;
import net.ihe.gazelle.app.dccvalidator.business.EncodedCBORDCC;
import net.ihe.gazelle.app.dccvalidator.business.EncodedCOSEDCC;
import net.ihe.gazelle.app.dccvalidator.business.EncodingException;
import net.ihe.gazelle.app.dccvalidator.business.ValidationException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.io.InputStream;
import java.security.InvalidAlgorithmParameterException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

public class COSEEncoderDecoderTest {

   private static final String TEST_STRING_CBOR = "test-string-cbor";
   private PrivateKey privateKey;
   private X509Certificate certificate;
   private List<X509Certificate> trustedCertificates;

   @BeforeEach
   public void setup() throws KeyStoreException, CertificateException, NoSuchAlgorithmException, IOException, UnrecoverableKeyException,
         InvalidAlgorithmParameterException {
      CryptoMaterial4TestLoader cryptoLoader = new CryptoMaterial4TestLoader();
      privateKey = cryptoLoader.getPrivateKey();
      certificate = cryptoLoader.getCertificate();
      trustedCertificates = cryptoLoader.getTrustedCertificates();
   }

   @Test
   public void dummyEncodeDecodeWithSignatureTest()
         throws CertificateException, EncodingException, IOException, ValidationException, DecodingException {

      // Create CWT, COSE and signature
      COSEEncoderDecoder codec = new COSEEncoderDecoder(privateKey, certificate, trustedCertificates);
      CBORObject cbor = CBORObject.FromObject(TEST_STRING_CBOR);
      EncodedCBORDCC cborDCC = new EncodedCBORDCC(cbor.EncodeToBytes(), Instant.now().plus(90, ChronoUnit.DAYS));
      EncodedCOSEDCC encoded = codec.encode(cborDCC);

      CBORObject cose = CBORObject.DecodeFromBytes(encoded.getEncodedCOSE());
      //FIXME add assertion on produced COSE

      // Decode CWT, COSE and verify signature, verify content match original.
      EncodedCBORDCC decoded = codec.decodeAndValidate(encoded);
      Assertions.assertEquals(TEST_STRING_CBOR, CBORObject.DecodeObjectFromBytes(decoded.getCborEncoded(), String.class));


   }

   //FIXME add test to detect signature corruption

   //FIXME add test to detect wrong CWT structure

   //FIXME add test to detect wrong COSE structure.

   private static InputStream getResourceAsStream(String ressourcePath) {
      return COSEEncoderDecoderTest.class.getClassLoader().getResourceAsStream(ressourcePath);
   }

}
