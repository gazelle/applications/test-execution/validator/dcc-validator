package net.ihe.gazelle.app.dccvalidator.adapter;

import net.ihe.gazelle.app.dccvalidator.business.CompressedZLibDCC;
import net.ihe.gazelle.app.dccvalidator.business.DecodingException;
import net.ihe.gazelle.app.dccvalidator.business.EncodedCOSEDCC;
import net.ihe.gazelle.app.dccvalidator.business.EncodingException;
import net.ihe.gazelle.app.dccvalidator.business.ValidationException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.io.InputStream;

public class ZLibEncoderDecoderTest {

   private byte[] resourceTestImage;
   private byte[] resourceTestCompressed;

   @BeforeEach
   public void setup() throws IOException {
      resourceTestImage = getResourceAsStream("barcode-test-no-prefix.png").readAllBytes();
      resourceTestCompressed = getResourceAsStream("barcode-test.zlib").readAllBytes();
   }

   @Test
   public void encodeSimpleTest() throws EncodingException, IOException {
      ZLibEncoderDecoder zLibCodec = new ZLibEncoderDecoder();
      CompressedZLibDCC encoded = zLibCodec.encode(new EncodedCOSEDCC(resourceTestImage));
      Assertions.assertArrayEquals(resourceTestCompressed, encoded.getContent());
   }

   @Test
   public void decodeSimpleTest() throws ValidationException, DecodingException {
      ZLibEncoderDecoder zLibCodec = new ZLibEncoderDecoder();
      EncodedCOSEDCC decoded = zLibCodec.decodeAndValidate(new CompressedZLibDCC(resourceTestCompressed));
      Assertions.assertArrayEquals(resourceTestImage, decoded.getEncodedCOSE());
   }

   private static InputStream getResourceAsStream(String ressourcePath) {
      return ZLibEncoderDecoderTest.class.getClassLoader().getResourceAsStream(ressourcePath);
   }
}


