package net.ihe.gazelle.app.dccvalidator.application;

import net.ihe.gazelle.app.dccvalidator.adapter.BarcodeEncoderDecoder;
import net.ihe.gazelle.app.dccvalidator.adapter.Base45EncoderDecoder;
import net.ihe.gazelle.app.dccvalidator.adapter.CBOREncoderDecoderV100;
import net.ihe.gazelle.app.dccvalidator.adapter.COSEEncoderDecoder;
import net.ihe.gazelle.app.dccvalidator.adapter.ZLibEncoderDecoder;
import net.ihe.gazelle.app.dccvalidator.business.BarcodeImageDCC;
import net.ihe.gazelle.app.dccvalidator.business.CompressedZLibDCC;
import net.ihe.gazelle.app.dccvalidator.business.EncodedBase45DCC;
import net.ihe.gazelle.app.dccvalidator.business.EncodedCBORDCC;
import net.ihe.gazelle.app.dccvalidator.business.EncodedCOSEDCC;
import net.ihe.gazelle.app.dccvalidator.business.EncodingException;
import net.ihe.gazelle.app.dccvalidator.business.v1_0_0.EuDgcBuilder;
import net.ihe.gazelle.modelapi.dcc.v1_0_0.Eudgc;
import net.ihe.gazelle.modelapi.validation.business.InvalidObjectException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.time.Instant;
import java.time.temporal.ChronoUnit;

public class BasicDCCValidatorTest {

   private BarcodeImageDCC testResourceBarcodeDCC;
   private CryptoMaterial4TestLoader cryptoLoader;

   @BeforeEach
   public void setup() throws EncodingException, CertificateException, UnrecoverableKeyException, NoSuchAlgorithmException, KeyStoreException,
         InvalidAlgorithmParameterException, IOException {
      cryptoLoader = new CryptoMaterial4TestLoader();

      // Create simple DCC certificate for validation testing
      Eudgc eudgc = new EuDgcBuilder().setPersonName("Forgeron", "Jean").setDateOfBirth(1980, 6, 3).build();
      EncodedCBORDCC cbor = new CBOREncoderDecoderV100().encode(eudgc);
      cbor.setExpiration(Instant.now().plus(90, ChronoUnit.DAYS));
      EncodedCOSEDCC cose = new COSEEncoderDecoder(cryptoLoader.getPrivateKey(), cryptoLoader.getCertificate(), null)
            .encode(cbor);
      CompressedZLibDCC compressed = new ZLibEncoderDecoder().encode(cose);
      EncodedBase45DCC base45 = new Base45EncoderDecoder().encode(compressed);
      testResourceBarcodeDCC = new BarcodeEncoderDecoder().encode(base45);
   }

   @Test
   public void simpleValidationTest() throws InvalidObjectException {
      AbstractValidator validator = new BasicDCCValidator(cryptoLoader.getTrustedCertificates(), null);
      Assertions.assertDoesNotThrow(() -> validator.validate(testResourceBarcodeDCC.getImageBytes()));
   }

}
