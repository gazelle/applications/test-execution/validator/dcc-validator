package net.ihe.gazelle.app.dccvalidator.application;

import java.io.IOException;
import java.io.InputStream;
import java.security.InvalidAlgorithmParameterException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.security.cert.PKIXParameters;
import java.security.cert.X509Certificate;
import java.util.List;
import java.util.stream.Collectors;

public class CryptoMaterial4TestLoader {

   private PrivateKey privateKey;
   private X509Certificate certificate;
   private List<X509Certificate> trustedCertificates;

   public CryptoMaterial4TestLoader() throws KeyStoreException, CertificateException, NoSuchAlgorithmException, IOException,
         UnrecoverableKeyException, InvalidAlgorithmParameterException {
      KeyStore keystore = KeyStore.getInstance("JKS");
      keystore.load(getResourceAsStream("keystore.jks"), "password".toCharArray());
      privateKey = (PrivateKey) keystore.getKey("junit", "password".toCharArray());
      certificate = (X509Certificate) keystore.getCertificate("junit");
      PKIXParameters pkixParam = new PKIXParameters(keystore);
      trustedCertificates = pkixParam.getTrustAnchors().stream().map(t -> t.getTrustedCert()).collect(Collectors.toList());
   }

   public PrivateKey getPrivateKey() {
      return privateKey;
   }

   public X509Certificate getCertificate() {
      return certificate;
   }

   public List<X509Certificate> getTrustedCertificates() {
      return trustedCertificates;
   }

   private static InputStream getResourceAsStream(String ressourcePath) {
      return CryptoMaterial4TestLoader.class.getClassLoader().getResourceAsStream(ressourcePath);
   }
}
