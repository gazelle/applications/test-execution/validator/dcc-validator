package net.ihe.gazelle.app.dccvalidator.adapter;

import net.ihe.gazelle.modelapi.validation.business.ValidationOverview;
import net.ihe.gazelle.modelapi.validation.business.ValidationReport;

import java.util.UUID;

public class ValidationReport4TestUtil {
   public ValidationReport4TestUtil() {
   }

   public ValidationReport createValidationReportForTest(Class testClass) {
      return new ValidationReport(UUID.randomUUID().toString(),
            new ValidationOverview("Use this report at your own risk.", "DCC Validation Service", "1.0.0-SNAPHOT", testClass.getName()));
   }
}