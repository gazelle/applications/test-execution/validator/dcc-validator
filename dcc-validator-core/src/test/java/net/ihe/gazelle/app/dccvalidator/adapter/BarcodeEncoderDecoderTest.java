package net.ihe.gazelle.app.dccvalidator.adapter;

import net.ihe.gazelle.app.dccvalidator.business.BarcodeImageDCC;
import net.ihe.gazelle.app.dccvalidator.business.DecodingException;
import net.ihe.gazelle.app.dccvalidator.business.EncodedBase45DCC;
import net.ihe.gazelle.app.dccvalidator.business.EncodingException;
import net.ihe.gazelle.app.dccvalidator.business.ValidationException;
import net.ihe.gazelle.modelapi.validation.business.ValidationReport;
import net.ihe.gazelle.modelapi.validation.business.ValidationTestResult;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.io.InputStream;

public class BarcodeEncoderDecoderTest {

   private static final String TEST_STRING = "https://lesjoiesducode.fr/code-fonctionne-premier-coup";

   private ValidationReport4TestUtil validationReport4TestUtil = new ValidationReport4TestUtil();
   BarcodeEncoderDecoder barcodeCodec = new BarcodeEncoderDecoder();
   private byte[] resourceTestImage;

   @BeforeEach
   public void setup() throws IOException {
      resourceTestImage = getResourceAsStream("barcode-test.png").readAllBytes();
   }

   @Test
   public void encodeSimpleTest() throws EncodingException, IOException {
      BarcodeImageDCC generatedImage = barcodeCodec.encode(new EncodedBase45DCC(TEST_STRING));
      Assertions.assertArrayEquals(resourceTestImage, generatedImage.getImageBytes());
   }

   @Test
   public void decodeSimpleTest() throws DecodingException, IOException, ValidationException {
      EncodedBase45DCC base45 = barcodeCodec.decodeAndValidate(new BarcodeImageDCC(resourceTestImage));
      Assertions.assertEquals(TEST_STRING, base45.getBase45string());
   }

   @Test
   public void decodeWithReportTest() throws DecodingException {
      ValidationReport report = validationReport4TestUtil.createValidationReportForTest(this.getClass());
      EncodedBase45DCC base45 = barcodeCodec.decodeAndReportValidation(new BarcodeImageDCC(resourceTestImage), report);
      Assertions.assertEquals(ValidationTestResult.PASSED, report.getValidationOverallResult());
      Assertions.assertEquals(1, report.getSubReports().size());
   }

   private static InputStream getResourceAsStream(String ressourcePath) {
      return BarcodeEncoderDecoderTest.class.getClassLoader().getResourceAsStream(ressourcePath);
   }
}
