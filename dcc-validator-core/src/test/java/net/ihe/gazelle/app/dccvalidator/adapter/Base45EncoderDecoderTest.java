package net.ihe.gazelle.app.dccvalidator.adapter;

import net.ihe.gazelle.app.dccvalidator.business.CompressedZLibDCC;
import net.ihe.gazelle.app.dccvalidator.business.DecodingException;
import net.ihe.gazelle.app.dccvalidator.business.EncodedBase45DCC;
import net.ihe.gazelle.app.dccvalidator.business.EncodingException;
import net.ihe.gazelle.app.dccvalidator.business.ValidationException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.nio.charset.StandardCharsets;

public class Base45EncoderDecoderTest {

   private static final byte[] TEST_BYTES = "https://lesjoiesducode.fr/quand-je-constate-que-jai-toujours-un-probleme-dencodage"
         .getBytes(StandardCharsets.UTF_8);
   private static final String TEST_BASE45 = "A9DIWENPEJ/5TVDQQE 3E6$CMVCUPC6VC/%5QJEKFECEC+TCCKD9X5-3E " +
         "QEIECQZCKFEQZC8KDFDDHWE/$E34E4LERX5Z-DV9ET3ETVDF$DAX51$CUPC2VC93D";

   @Test
   public void encodeSimpleTest() throws EncodingException {
      Base45EncoderDecoder base45codec = new Base45EncoderDecoder();
      EncodedBase45DCC encoded = base45codec.encode(new CompressedZLibDCC(TEST_BYTES));
      Assertions.assertEquals(TEST_BASE45, encoded.getBase45string());
   }

   @Test
   public void decodeSimpleTest() throws ValidationException, DecodingException {
      Base45EncoderDecoder base45codec = new Base45EncoderDecoder();
      CompressedZLibDCC decoded = base45codec.decodeAndValidate(new EncodedBase45DCC(TEST_BASE45));
      Assertions.assertArrayEquals(TEST_BYTES, decoded.getContent());
   }

}
