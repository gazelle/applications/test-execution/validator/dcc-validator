package net.ihe.gazelle.app.dccvalidator.adapter;

import net.ihe.gazelle.app.dccvalidator.business.DecodingException;
import net.ihe.gazelle.app.dccvalidator.business.EncodedCBORDCC;
import net.ihe.gazelle.app.dccvalidator.business.EncodingException;
import net.ihe.gazelle.app.dccvalidator.business.ValidationException;
import net.ihe.gazelle.app.dccvalidator.business.v1_3_0.EuDccBuilder;
import net.ihe.gazelle.modelapi.dcc.v1_3_0.TestEntry;
import net.ihe.gazelle.modelapi.dcc.v1_3_0.Eudcc;
import net.ihe.gazelle.modelapi.validation.business.ConstraintValidation;
import net.ihe.gazelle.modelapi.validation.business.ValidationReport;
import net.ihe.gazelle.modelapi.validation.business.ValidationTestResult;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.opentest4j.AssertionFailedError;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.time.Instant;
import java.util.Comparator;
import java.util.List;

public class CBOREncoderDecoderV130Test {

   private final ValidationReport4TestUtil validationReport4TestUtil = new ValidationReport4TestUtil();
   private Eudcc testResourceDGC;
   private byte[] testResourceCBORDGC;
   private CBOREncoderDecoderV130 codec;

   @BeforeEach
   public void setUp() throws IOException {
      testResourceDGC = new EuDccBuilder().setPersonName("Forgeron", "Jean").setDateOfBirth(1978, 01, 01).build();
      testResourceCBORDGC = getResourceAsStream("dcc-test-1.3.0.cbor").readAllBytes();
      codec = new CBOREncoderDecoderV130(ValueSetProviderForUT.getDccValueSets());
   }

   @Test
   public void simpleEncodeTest() throws EncodingException, IOException {
      EncodedCBORDCC encoded = codec.encode(testResourceDGC);
      Assertions.assertArrayEquals(testResourceCBORDGC, encoded.getCborEncoded());
   }

   @Test
   public void simpleDecodeTest() throws IOException, ValidationException, DecodingException {
      Eudcc decodedDGC = codec.decodeAndValidate(new EncodedCBORDCC(testResourceCBORDGC, null));
      Assertions.assertEquals(testResourceDGC, decodedDGC);
   }

   @Test
   public void simpleDecodeWithReportTest() throws DecodingException {
      final ValidationReport report = validationReport4TestUtil.createValidationReportForTest(this.getClass());
      Eudcc decodedDGC = codec.decodeAndReportValidation(new EncodedCBORDCC(testResourceCBORDGC, null), report);
      Assertions.assertEquals(ValidationTestResult.PASSED, report.getValidationOverallResult());
      Assertions.assertNotNull(report.getSubReports().stream().filter(sr -> sr.getName().equals("CBOR Validation")).findFirst().orElseThrow(
            AssertionFailedError::new));
      Assertions.assertNotNull(report.getSubReports().stream().filter(sr -> sr.getName().equals("JSON Validation")).findFirst().orElseThrow(
            AssertionFailedError::new));
   }

   @Test
   public void decodeCborWithReportErrorTest() {
      final ValidationReport report = validationReport4TestUtil.createValidationReportForTest(this.getClass());

      Assertions.assertThrows(DecodingException.class,
            () -> codec.decodeAndReportValidation(new EncodedCBORDCC("This is not a CBOR".getBytes(StandardCharsets.UTF_8), null), report));

      assertReport(report, ValidationTestResult.UNDEFINED, ValidationTestResult.FAILED, ValidationTestResult.UNDEFINED, ValidationTestResult.UNDEFINED);
   }

   @Test
   public void valueSetsWithReportErrorTest() throws EncodingException, DecodingException {
      // Build a DCC version 1.0.0 with wrong tg value sets (COVID-19 instead of 840539006) to trigger an error
      TestEntry testEntry = new TestEntry("COVID-19", "LP217198-3", null, "2108", Instant.now(), "260415000", "CG Center", "FR",
            "https://gazelle.ihe.net/Fakelab4test", "URN:UVCI:01:FR:123/123456789");
      testResourceDGC.setT(List.of(testEntry));
      EncodedCBORDCC encodedCBORDCC = codec.encode(testResourceDGC);

      final ValidationReport report = validationReport4TestUtil.createValidationReportForTest(this.getClass());

      codec.decodeAndReportValidation(encodedCBORDCC, report);
      assertReport(report, ValidationTestResult.FAILED, ValidationTestResult.PASSED, ValidationTestResult.PASSED, ValidationTestResult.FAILED);
   }

   @Test
   public void valueSetsWithReportPassedTest() throws EncodingException, DecodingException {
      // Build a DCC version 1.0.0 with wrong tg value sets (COVID-19 instead of 840539006) to trigger an error
      TestEntry testEntry = new TestEntry("840539006", "LP217198-3", null, "2108", Instant.now(), "260415000", "CG Center", "FR",
              "https://gazelle.ihe.net/Fakelab4test", "URN:UVCI:01:FR:123/123456789");
      testResourceDGC.setT(List.of(testEntry));
      EncodedCBORDCC encodedCBORDCC = codec.encode(testResourceDGC);

      final ValidationReport report = validationReport4TestUtil.createValidationReportForTest(this.getClass());

      codec.decodeAndReportValidation(encodedCBORDCC, report);
      assertReport(report, ValidationTestResult.PASSED, ValidationTestResult.PASSED, ValidationTestResult.PASSED, ValidationTestResult.PASSED);
   }

   private void assertReport(ValidationReport report, ValidationTestResult reportResult, ValidationTestResult cborConstraintResult, ValidationTestResult jsonConstraintResult, ValidationTestResult valueSetConstraintResult) {
      Assertions.assertEquals(reportResult, report.getValidationOverallResult());
      Assertions.assertEquals(cborConstraintResult,
            report.getSubReports().stream().filter(sr -> sr.getName().equals("CBOR Validation")).findFirst()
                  .orElseThrow(AssertionFailedError::new)
                  .getConstraints().stream().filter(c -> c.getConstraintDescription().contains("CBOR Encoding")).findFirst()
                  .orElseThrow(AssertionFailedError::new)
                  .getTestResult());
      Assertions.assertEquals(jsonConstraintResult,
            report.getSubReports().stream().filter(sr -> sr.getName().equals("JSON Validation")).findFirst()
                  .orElseThrow(AssertionFailedError::new)
                  .getConstraints().stream().filter(c -> c.getConstraintDescription().contains("JSON Schema")).findFirst()
                  .orElseThrow(AssertionFailedError::new)
                  .getTestResult());
      Assertions.assertEquals(valueSetConstraintResult,
              report.getSubReports().stream().filter(sr -> sr.getName().equals("JSON Validation")).findFirst()
                      .orElseThrow(AssertionFailedError::new)
                      .getConstraints().stream().filter(c -> c.getConstraintDescription().contains("value set")).sorted(Comparator.comparing(ConstraintValidation::getTestResult).reversed()).findFirst()
                      .orElseThrow(AssertionFailedError::new)
                      .getTestResult());
   }

   private static InputStream getResourceAsStream(String ressourcePath) {
      return CBOREncoderDecoderV130Test.class.getClassLoader().getResourceAsStream(ressourcePath);
   }

}
