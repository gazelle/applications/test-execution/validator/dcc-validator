# EU-Digital-COVID-certificate Validator

The EU digital-covid-certificate Validator is an open-source Validation Service (under Apache 2
licence) provided by IHE Europe Gazelle Test Bed for verifying conformity of generated certificates
to the EU specifications.

**The Validator must NOT be used as-it-is as Verifier Application (See DCCA Architecture).**

**DCC Validator is currently a prototype and can neither be used for interoperability tests nor
conformity tests**

## Dependencies

DCC Validator is running on Debian, with Java 11, Jakarta EE 8 on Wildfly 18 or above.

**In order to have responding web-service, the current prototype requires a 
SNAPSHOT of the project `validation-model` to be build from the branch `feature/dirty-devs-for-DCC-prorotype`**

## Build projet

```shell
mvn clean package
```

## Environment setup

The application requires to aggregate all trusted signer's certificates
in `/opt/dcc-validator/truststore.jks` with password `password`.

## Wildfly Deployment

```shell
cp dcc-validator-service/target/app.dcc-validator-service-1.0.0-SNAPSHOT.war ${WILDFLY_HOME}/standalone/deployments/
```

## Docker Deployment

### Build image

```shell
docker build -t dcc-validator-on-wildfly-1.0.0-snapshot .
```

### Deploy

```shell
docker run -p 8080:8080 -v /opt/dcc-validator:/opt/dcc-validator --name dcc-validator dcc-validator-on-wildfly-1.0.0-snapshot 
```

## Web Service

Once deployed, the web-service can be accessed
at [http://localhost:8080/dcc-validator/dccValidationService?wsdl](http://localhost:8080/dcc-validator/dccValidationService?wsdl)